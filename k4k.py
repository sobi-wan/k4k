##
# K4K - Kalkulator 4 Kasia
##


def add(add_value1, add_value2):
    return add_value1 + add_value2


def substract(substract_value1, substract_value2):
    return substract_value1 - substract_value2


def multiply(multiply_value1, multiply_value2):
    return multiply_value1 * multiply_value2


def divide(divide_value1, divide_value2):
    if divide_value2 == 0:
        print("You can't divide by zero, dummy :)")
    else:
        return divide_value1 / divide_value2


def k4k(k4k_val1, k4k_val2, k4k_action):
    switcher = {
        "+": add(k4k_val1, k4k_val2,),
        "-": substract(k4k_val1, k4k_val2,),
        "*": multiply(k4k_val1, k4k_val2,),
        "/": divide(k4k_val1, k4k_val2,)
    }
    k4k_result = switcher.get(k4k_action)
    print("Result:", k4k_result)


def ask_val1():
    while True:
        try:
            ask_val1 = float(input("Val #1: "))
            return(ask_val1)
            break
        except ValueError:
            print("Watch for typos for val1!")


def ask_val2():
    while True:
        try:
            ask_val2 = float(input("Val #2: "))
            return(ask_val2)
            break
        except ValueError:
            print("Watch for typos for val2!")


def ask_action():
    while True:
        ask_actions = str("+-*/")
        ask_action = input(f"({ask_actions}): ")

        if ask_action not in ask_actions:
            print("Watch for typos for action!")
        else:
            return(ask_action)
            break


def end():
    cont = input("Do you want to end? (y/n): ")

    if cont in "Yy":
        print("good bye!")
        return 0
    elif cont in "Nn":
        print("good, let's try again :)")
        engine()
    else:
        print("hey! watch your fingers!")
        end()


def engine():
    value1 = ask_val1()
    value2 = ask_val2()
    action = ask_action()
    k4k(value1, value2, action)
    end()


engine()
